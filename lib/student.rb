class Student
  attr_reader :first_name, :last_name, :courses, :students

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    
  end
  
  def name
    first_name + " " + last_name
  end
  
  def enroll(course)
    unless courses.include?(course)
      if courses.any? {|cour| course.conflicts_with?(cour)} 
        raise "Yo you got a class at that time, bruh"
      else
        self.courses << course
        course.students << self
      end
    end
  end
  
  def course_load
    hsh = Hash.new(0)
    self.courses.each do |course, credits|
      hsh[course.department] += course.credits
    end
    hsh
  end
end
